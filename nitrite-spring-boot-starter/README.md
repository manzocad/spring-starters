# Nitrite Spring Boot Starter

This starter add Spring Boot autoconfiguration along with Maven
dependencies for [Nitrite NOSQL Database](http://www.dizitart.org/nitrite-database.html)
from Dizitart.

## POM configuration

    <!-- Add Manzocad repository to pom -->
    <repositories>
        <repository>
            <id>bintray-manzo-giuseppe-manzocad-maven-repository</id>
            <url>https://dl.bintray.com/manzo-giuseppe/manzocad-maven-repository</url>
        </repository>
    </repositories>

    <dependency>
        <groupId>org.manzocad</groupId>
        <artifactId>nitrite-spring-boot-starter</artifactId>
        <version>${starter.version}</version>
    </dependency>

## Spring Boot configuration
You can specify database path in application.properties:

    manzocad.nitrite.database=/path/to/nitrite.db

If you want a temporary in-memory database

    manzocad.nitrite.database=:memory:

## Java 8 Date and Time support
If com.fasterxml.jackson.datatype:jackson-datatype-jsr310 is added
dependencies java 1.8 datetime feature will be enabled.
You can further set formats with the following options (with defaults):

    manzocad.nitrite.dateformat=yyyy-MM-dd
    manzocad.nitrite.timeformat=HH:mm
    manzocad.nitrite.datetimeformat=yyyy-MM-dd HH:mm