package org.manzocad.spring.starters.nitrite;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("manzocad.nitrite")
public class NitriteProperties {

    private String database = ":memory:";

    private String dateformat = "yyyy-MM-dd";

    private String timeformat = "HH:mm";

    private String datetimeformat = "yyyy-MM-dd HH:mm";

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getDateformat() {
        return dateformat;
    }

    public void setDateformat(String dateformat) {
        this.dateformat = dateformat;
    }

    public String getTimeformat() {
        return timeformat;
    }

    public void setTimeformat(String timeformat) {
        this.timeformat = timeformat;
    }

    public String getDatetimeformat() {
        return datetimeformat;
    }

    public void setDatetimeformat(String datetimeformat) {
        this.datetimeformat = datetimeformat;
    }
}
