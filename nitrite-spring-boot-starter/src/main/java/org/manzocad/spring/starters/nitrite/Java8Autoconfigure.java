package org.manzocad.spring.starters.nitrite;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;

@Configuration
@EnableConfigurationProperties(NitriteProperties.class)
@ConditionalOnClass(JavaTimeModule.class)
@AutoConfigureBefore(NitriteAutoconfigure.class)
public class Java8Autoconfigure {

    @Autowired
    private NitriteProperties conf;

    @Bean
    @ConditionalOnMissingBean
    public JavaTimeModule getModule(){
        JavaTimeModule module = new JavaTimeModule();

        DateTimeFormatter DATEFORMAT = DateTimeFormatter.ofPattern(conf.getDateformat());
        DateTimeFormatter TIMEFORMAT = DateTimeFormatter.ofPattern(conf.getTimeformat());
        DateTimeFormatter DATETIMEFORMAT = DateTimeFormatter.ofPattern(conf.getDatetimeformat());

        module.addSerializer(LocalDate.class, new JsonSerializer<LocalDate>() {
            @Override
            public void serialize(LocalDate localDate, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
                gen.writeString(localDate.format(DATEFORMAT));
            }
        });
        module.addDeserializer(LocalDate.class, new JsonDeserializer<LocalDate>() {
            @Override
            public LocalDate deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                return LocalDate.parse(parser.getValueAsString(), DATEFORMAT);
            }
        });

        module.addSerializer(LocalTime.class, new JsonSerializer<LocalTime>() {
            @Override
            public void serialize(LocalTime localTime, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
                gen.writeString(localTime.format(TIMEFORMAT));
            }
        });
        module.addDeserializer(LocalTime.class, new JsonDeserializer<LocalTime>() {
            @Override
            public LocalTime deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                return LocalTime.parse(parser.getValueAsString(), TIMEFORMAT);
            }
        });

        module.addSerializer(LocalDateTime.class, new JsonSerializer<LocalDateTime>() {
            @Override
            public void serialize(LocalDateTime localDateTime, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
                gen.writeString(localDateTime.format(DATETIMEFORMAT));
            }
        });
        module.addDeserializer(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
            @Override
            public LocalDateTime deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                return LocalDateTime.parse(parser.getValueAsString(), DATETIMEFORMAT);
            }
        });

        return module;
    }

}
