package org.manzocad.spring.starters.nitrite;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.mapper.JacksonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(NitriteProperties.class)
public class NitriteAutoconfigure {

    @Autowired
    private NitriteProperties conf;

    @Autowired(required = false)
    private JavaTimeModule module;

    @Bean
    @ConditionalOnMissingBean
    public Nitrite getDatabase(){
        JacksonMapper mapper = new JacksonMapper();
        mapper.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        if(module != null){
            mapper.getObjectMapper().registerModule(module);
        }

        if(conf.getDatabase().equals(":memory:")){
            return Nitrite
                .builder()
                .nitriteMapper(mapper)
                .openOrCreate();
        }else{
            return Nitrite
                .builder()
                .nitriteMapper(mapper)
                .filePath(conf.getDatabase())
                .openOrCreate();
        }
    }

}
