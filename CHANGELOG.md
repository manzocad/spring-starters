# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# [1.4.0] 2018-03-11
## Changed
- support for java 8 datetime features made mandatory

# [1.3.0] 2018-05-12
## Changed
- manzocad dependencies version 1.3.0

# [1.2.0] 2018-03-11
## Changed
- version 1.2.0

# [1.1.0] 2018-03-11
## Added
- support for java 8 datetime features to nitrite starter

# [0.4.0] - 2018-03-05
## Added
- shiro-adrealm-spring-boot-starter

# [0.2.0] - 2018-03-05
## Added
- nitrite-spring-boot-starter
- shiro-spring-boot-starter