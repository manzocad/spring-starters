# Manzocad Spring Boot Starters

This project aggregates all spring Boot starters from Manzocad. All
subprojects shares the same version number of this parent that will
be incremented accordingly

## POM configuration

    <!-- Add Manzocad repository to pom -->
    <repositories>
        <repository>
            <id>bintray-manzo-giuseppe-manzocad-maven-repository</id>
            <url>https://dl.bintray.com/manzo-giuseppe/manzocad-maven-repository</url>
        </repository>
    </repositories>

    <!-- Add as many starters as you want  -->
    <dependency>
        <groupId>org.manzocad</groupId>
        <artifactId>${starter.name}</artifactId>
        <version>${starter.version}</version>
    </dependency>

## Available starters
- [nitrite-spring-boot-starter](nitrite-spring-boot-starter/README.md)
- [shiro-spring-boot-starter](shiro-spring-boot-starter/README.md)
- [shiro-adrealm-spring-boot-starter](shiro-adrealm-spring-boot-starter/README.md)
