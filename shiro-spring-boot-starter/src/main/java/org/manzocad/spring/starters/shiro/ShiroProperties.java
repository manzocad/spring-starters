package org.manzocad.spring.starters.shiro;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;

@ConfigurationProperties("manzocad.shiro")
public class ShiroProperties {

    private String username = "admin";
    private String password = "admin";

    private LinkedHashMap<String,String> paths = new LinkedHashMap<String, String>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LinkedHashMap<String, String> getPaths() {
        return paths;
    }

    public void setPaths(LinkedHashMap<String, String> paths) {
        this.paths = paths;
    }
}
