package org.manzocad.spring.starters.shiro;

import org.apache.shiro.realm.Realm;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAutoConfiguration;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ShiroProperties.class)
@AutoConfigureBefore(ShiroAutoConfiguration.class)
public class ShiroRealmAutoconfigure {

    @Autowired
    private ShiroProperties conf;

    @Bean
    @ConditionalOnMissingBean(Realm.class)
    public Realm getDefaultRealm(){
        SimpleAccountRealm realm = new SimpleAccountRealm();
        realm.addAccount(conf.getUsername(), conf.getPassword());
        return realm;
    }

}
