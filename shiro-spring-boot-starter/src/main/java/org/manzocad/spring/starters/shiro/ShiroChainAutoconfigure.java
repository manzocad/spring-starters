package org.manzocad.spring.starters.shiro;

import org.apache.shiro.spring.config.web.autoconfigure.ShiroWebAutoConfiguration;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Map;

@Configuration
@EnableConfigurationProperties(ShiroProperties.class)
@AutoConfigureBefore(ShiroWebAutoConfiguration.class)
public class ShiroChainAutoconfigure {

    @Autowired
    private ShiroProperties conf;

    @Bean
    @ConditionalOnMissingBean(ShiroFilterChainDefinition.class)
    public ShiroFilterChainDefinition getDefaultFilterChain(){
        DefaultShiroFilterChainDefinition d = new DefaultShiroFilterChainDefinition();

        ListIterator<Map.Entry<String, String>> iterator = new ArrayList<Map.Entry<String, String>>(conf.getPaths().entrySet()).listIterator(conf.getPaths().size());
        while (iterator.hasPrevious()){
            Map.Entry<String, String> entry = iterator.previous();
            d.addPathDefinition(entry.getKey(), entry.getValue());
        }
        return d;
    }

}
