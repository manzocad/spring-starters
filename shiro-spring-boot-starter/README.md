# Shiro Spring Boot Starter

This starter add Spring Boot autoconfiguration along with Maven
dependencies for [Apache Shiro](https://shiro.apache.org/) authentication
and authorization framework from Apache Foundation.

## POM configuration

    <!-- Add Manzocad repository to pom -->
    <repositories>
        <repository>
            <id>bintray-manzo-giuseppe-manzocad-maven-repository</id>
            <url>https://dl.bintray.com/manzo-giuseppe/manzocad-maven-repository</url>
        </repository>
    </repositories>

    <dependency>
        <groupId>org.manzocad</groupId>
        <artifactId>shiro-spring-boot-starter</artifactId>
        <version>${starter.version}</version>
    </dependency>

## Spring Boot configuration
If none Shiro Realm.class beans has bean instantiated, then
a SimpleAccountRealm will be instantiated automatically, with the following
username and password defaults, which you can modify in application.properties:

    manzocad.shiro.username=admin
    manzocad.shiro.password=admin

If none ShiroFilterChainDefinition has been instantiated, then
an DefaultShiroFilterChainDefinition will be instantiated. You can
define paths in application.dependencies;
[order is important!](https://shiro.apache.org/web.html#urls-)

    manzocad.shiro.paths./api/**=noSessionCreation,authcBasic
    manzocad.shiro.paths./**=authc

