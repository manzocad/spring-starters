# Shiro Spring Boot Starter

This starter add Spring Boot autoconfiguration along with Maven
dependencies for an Active Directory Realm
for [Apache Shiro](https://shiro.apache.org/) authentication

## POM configuration

    <!-- Add Manzocad repository to pom -->
    <repositories>
        <repository>
            <id>bintray-manzo-giuseppe-manzocad-maven-repository</id>
            <url>https://dl.bintray.com/manzo-giuseppe/manzocad-maven-repository</url>
        </repository>
    </repositories>

    <dependency>
        <groupId>org.manzocad</groupId>
        <artifactId>shiro-adrealm-spring-boot-starter</artifactId>
        <version>${starter.version}</version>
    </dependency>

## Spring Boot configuration
The minimum configuration requires to AD realm to be enabled and the
ad server hostname or address to be specified

    manzocad.shiro.ad.enabled = true
    manzocad.shiro.ad.server = adserver.localdomain

Further configuration are available to set server port and enable ssl

    manzocad.shiro.ad.port = 389
    manzocad.shiro.ad.ssl = true

Finally most likely you want to set a specific Bind DN format. Just put '%s'
wherever you want to fill in with the given username. Default is just the username

    manzocad.shiro.ad.binddn = uid=%s,dc=example,dc=com

