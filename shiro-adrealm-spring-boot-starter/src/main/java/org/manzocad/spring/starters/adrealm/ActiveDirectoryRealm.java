/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.manzocad.spring.starters.adrealm;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.AuthenticatingRealm;

/**
 *
 * @author MANZOGI9
 */
public class ActiveDirectoryRealm extends AuthenticatingRealm{
    
    private String serverName = null;
    private String serverPort = "389";
    private String BindDNFormat = "rete\\%s";
    private boolean useSSL = false;

    public void setUseSSL(boolean useSSL) {
        this.useSSL = useSSL;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public String getBindDNFormat() {
        return BindDNFormat;
    }

    public void setBindDNFormat(String BindDNFormat) {
        this.BindDNFormat = BindDNFormat;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
                
        UsernamePasswordToken t = (UsernamePasswordToken) token;
        DirContext ctx = null;
        SimpleAuthenticationInfo info = null;
        
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        
        if( useSSL ){
            env.put(Context.SECURITY_PROTOCOL, "ssl");
            
        }
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_CREDENTIALS, String.format(BindDNFormat, t.getUsername()));
                 
        env.put(Context.PROVIDER_URL, String.format("ldap://%s:%s", serverName,serverPort));
        
        try {
            ctx = new InitialDirContext(env);
            Logger.getLogger(ActiveDirectoryRealm.class.getName()).info("Authenticated user[" + t.getUsername()+"] with domain controller["+serverName+"]");
            info = new SimpleAuthenticationInfo(t.getPrincipal(), t.getPassword(), "ADRealm");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return info;        
    }

    /*@Override
    public String toString() {
        return "ActiveDirectoryRealm{" +
            "serverName='" + serverName + '\'' +
            ", serverPort='" + serverPort + '\'' +
            ", BindDNFormat='" + BindDNFormat + '\'' +
            ", useSSL=" + useSSL +
            "} " + super.toString();
    }*/
}
