package org.manzocad.spring.starters.adrealm;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("manzocad.shiro.ad")
public class ADProperties {

    private String server;
    private String port = "389";
    private String binddn = "%s";
    private boolean ssl = false;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getBinddn() {
        return binddn;
    }

    public void setBinddn(String binddn) {
        this.binddn = binddn;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    /*
    @Override
    public String toString() {
        return "ADProperties{" +
            "server='" + server + '\'' +
            ", port='" + port + '\'' +
            ", binddn='" + binddn + '\'' +
            ", ssl=" + ssl +
            '}';
    }*/
}
