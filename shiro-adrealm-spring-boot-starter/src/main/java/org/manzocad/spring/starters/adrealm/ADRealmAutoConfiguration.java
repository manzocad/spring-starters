package org.manzocad.spring.starters.adrealm;

import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Configuration
@EnableConfigurationProperties(ADProperties.class)
@AutoConfigureBefore(ShiroAutoConfiguration.class)
public class ADRealmAutoConfiguration {

    @Autowired
    private ADProperties conf;

    @Bean
    @ConditionalOnProperty(prefix = "manzocad.shiro.ad", name="enabled", havingValue = "true", matchIfMissing = false)
    public Realm getAdRealm(){
        Logger.getLogger(getClass().getName()).info("Active Directory Realm Instance");
        ActiveDirectoryRealm realm = new ActiveDirectoryRealm();
        realm.setServerName(conf.getServer());
        realm.setServerPort(conf.getPort());
        realm.setBindDNFormat(conf.getBinddn());
        realm.setUseSSL(conf.isSsl());

        /*Logger.getLogger(getClass().getName()).info(conf.toString());
        Logger.getLogger(getClass().getName()).info(realm.toString());*/

        return realm;
    }



}
